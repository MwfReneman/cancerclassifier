/**
 * This program is made during and outside the lessons given on the study Bio-informatica on the Hanzehogeschool Groningen.
 * The meaning of this program is to use machine learning algorihms to predict if an cancercell is malignant or benign.
 * If u need help with running the program use -h or -HELP
 *
 * Copyright (c) 2019 Max Reneman
 * All rights reserved
 */


public class CommandLineMain {


    public static void main(String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }

        } catch (IllegalStateException ex){

            //TODO catch exceptions

        }
    }
}
