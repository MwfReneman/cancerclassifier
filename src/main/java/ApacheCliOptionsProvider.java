/*
 * Copyright (c) 2019 Max Reneman
 * All rights reserved
 */

import org.apache.commons.cli.*;


import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * This class is used to get the data from the commandline or from the given file.
 * Afterwards this program will call on the WekaRunner to start the classification of the newly given instances
 */


public class ApacheCliOptionsProvider implements OptionsProvider{

    private static final String INPUT = "input";
    private static final String FILE = "file";
    private static final String HELP = "help";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private String file;


    public ApacheCliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    private void initialize() {
        buildOptions();
        processCommandline();
    }
    public Boolean helpRequested() {
        /**
         * This will return if help was requested
         */
        return this.commandLine.hasOption(HELP);
    }

    private void buildOptions(){
        /**
         * Building the apache commandline options
         */
        this.options = new Options();
        Option helpOption = new Option("h",HELP, false, "Get the help message");
        Option inputOption = new Option("i",INPUT, true, "single input in string(not file), example: -i 84300903,M,19.69,21.25,130,1203,0.1096,0.1599,0.1974,0.1279,0.2069,0.05999,0.7456,0.7869,4.585,94.03,0.00615,0.04006,0.03832,0.02058,0.0225,0.004571,23.57,25.53,152.5,1709,0.1444,0.4245,0.4504,0.243,0.3613,0.08758");
        Option inputOptionFile = new Option("f",FILE, true, "input .arff file, example: -f \"testdata/unknownTestData.arff\"");

        options.addOption(helpOption);
        options.addOption(inputOption);
        options.addOption(inputOptionFile);
    }

    private void processCommandline(){
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
            if (this.commandLine.hasOption(FILE)) {

                WekaRunner.main(this.commandLine.getOptionValues(FILE),"file");
            }

            if (commandLine.hasOption(INPUT)) {
                String s = commandLine.getOptionValue(INPUT).trim();
                if (isLegalInput(s)) {
                    System.out.println("the input is legal");
                    WekaRunner.main(this.commandLine.getOptionValues(INPUT), "input");

                    //TODO run the cancer classifier


                } else {
                    throw new IllegalArgumentException("This input is illegal");
                }

                }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isLegalInput(String input) {
        /**
         * This method will check if the given in put is the same as the expected input
         */
         Pattern pattern = Pattern.compile("[a-zA-Z]");

        String[][] splitInput = new String[][]{
                input.split(",")
        };
        String[] splitInput1 = splitInput[0];

        if (splitInput1.length == 32){
            if (!Arrays.asList(splitInput1).contains(pattern)) {
                //TODO regex checker not accurate
                for (String i : splitInput1 ) {
                    //System.out.println(i);
                }
                return true;
            }
            else System.out.println("the input contains something else then digits");

            }
        else
            System.out.println("the input has the incorrect amount of value's \n have u made sure to put 32 inputs?\n " +
                    "for help use the '-help' command");
            return false;



    }
    void printHelp() {
        /**
         * This method will display the help function
         */
         HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("\t\t\tCancerClassifier\n\nThis is a machine learning program used to predict if an cancercell is malignant or benign.\n Options:\n", options);
        System.out.println("The data entered should have the following attributes in the following order: attribute1 = @attribute ' diagnosis' {M,B,?}\n" +
                "attribute2 = @attribute ' radius_mean' numeric\n" +
                "attribute3 = @attribute ' texture_mean' numeric\n" +
                "attribute4 = @attribute ' perimeter_mean' numeric\n" +
                "attribute5 = @attribute ' area_mean' numeric\n" +
                "attribute6 = @attribute ' smoothness_mean' numeric\n" +
                "attribute7 = @attribute ' compactness_mean' numeric\n" +
                "attribute8 = @attribute ' concavity_mean' numeric\n" +
                "attribute9 = @attribute ' concave_points_mean' numeric\n" +
                "attribute10 = @attribute ' symmetry_mean' numeric\n" +
                "attribute11 = @attribute ' fractal_dimention_mean' numeric\n" +
                "attribute12 = @attribute ' radius_SE' numeric\n" +
                "attribute13 = @attribute ' texture_SE' numeric\n" +
                "attribute14 = @attribute ' perimeter_SE' numeric\n" +
                "attribute15 = @attribute ' area_SE' numeric\n" +
                "attribute16 = @attribute ' smoothness_SE' numeric\n" +
                "attribute17 = @attribute ' compactness_SE' numeric\n" +
                "attribute18 = @attribute ' concavity_SE' numeric\n" +
                "attribute19 = @attribute ' concave_points_SE' numeric\n" +
                "attribute20 = @attribute ' symmetry_SE' numeric\n" +
                "attribute21 = @attribute ' fractal_dimention_SE' numeric\n" +
                "attribute22 = @attribute ' radius_worst' numeric\n" +
                "attribute23 = @attribute ' texture_worst' numeric\n" +
                "attribute24 = @attribute ' perimeter_worst' numeric\n" +
                "attribute25 = @attribute ' area_worst' numeric\n" +
                "attribute26 = @attribute ' smoothness_worst' numeric\n" +
                "attribute27 = @attribute ' compactness_worst' numeric\n" +
                "attribute28 = @attribute ' concavity_worst' numeric\n" +
                "attribute29 = @attribute ' concave_points_worst' numeric\n" +
                "attribute30 = @attribute ' symmetry_worst' numeric\n" +
                "attribute31 = @attribute ' fractal_dimention_worst' numeric");
    }

    @Override
    public String input() {
        return null; //this.input;
    }
}
