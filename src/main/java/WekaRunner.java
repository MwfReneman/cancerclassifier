/*
 * Copyright (c) 2019 Max Reneman
 * All rights reserved
 */

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.classifiers.functions.SMO;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.io.PrintWriter;


/**
 * This Class contains the code necessary to run the classification of the new instances
 */

class WekaRunner {
    private final String modelFile = "model/svm(SMO).model";

    static void main(String[] args, String inputType) {
        /**
         * start the wekarunner
         */
         WekaRunner runner = new WekaRunner();
        runner.start(args, inputType);
    }


    private void start(String[] args, String inputType) {
        String dataInputFile = "testdata/wdbc_data.arff";
        String dataUnknownFile = args[0];
        System.out.println(inputType);
        Instances unknownInstances = null;
        try {
            Instances instances = loadArff(dataInputFile);
            Instances instancesRemoved = removeAttributes(instances);
            SMO smo = buildClassifier(instancesRemoved);
            weka.core.SerializationHelper.write(modelFile, smo);
            SMO fromFile = loadClassifier();
            if (inputType.equals("file")){
                unknownInstances = loadArff(dataUnknownFile);
                Instances unknownInstancesRemoved = removeAttributes(unknownInstances);
                Instances classifiedOutput = classifyNewInstances(fromFile, unknownInstancesRemoved);
                printOutput(classifiedOutput, unknownInstances);
            }else {unknownInstances = csvWriter(dataUnknownFile);
                    Instances unknownInstancesRemoved = removeAttributes(unknownInstances);
                    unknownInstancesRemoved.setClassIndex(0);
                    Instances classifiedOutput = classifyNewInstances(fromFile, unknownInstancesRemoved);
                    printOutput(classifiedOutput, unknownInstances);



            }


        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    private void printOutput(Instances classifiedOutput, Instances unclassifiedInput) {
        /**
         * This method displays the output of the machine learning classification
         */
        System.out.println("\n#################################################\n");
        System.out.println("\t\t\t The program has finished running...\n");
        System.out.println("u have entered "+ classifiedOutput.numInstances()+" instances.");
        //System.out.println(classifiedOutput.classAttribute());
        for (int i = 0; i < classifiedOutput.numInstances(); i++){
            System.out.println("input number: " + (i+1));
            System.out.println(unclassifiedInput.instance(i));
            double[] designation = (classifiedOutput.instance(i).toDoubleArray());

            if (designation[24]==1){
                System.out.println("The designation is malignant\n");
            }
            else {
                System.out.println("the designation is benign\n");
            }


        }

    }

    private Instances classifyNewInstances(SMO fromFile, Instances unknownInstances) throws Exception{
        /**
         * This method classifies the newly given instances
         */
        //create copy
        Instances labeled = new Instances(unknownInstances);
        //Predict instances
        for (int i = 0; i < unknownInstances.numInstances(); i++){
            System.out.println(unknownInstances.instance(i));
            double classLabel = fromFile.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(classLabel);
        }
        return labeled;
    }

    private SMO loadClassifier() throws Exception {
        return (SMO) weka.core.SerializationHelper.read(modelFile);
    }

    private SMO buildClassifier(Instances instances) throws Exception {
        instances.setClassIndex(0);
        SMO supportVectorMachine = new SMO();
        supportVectorMachine.buildClassifier(instances);
        return supportVectorMachine;
    }

    private Instances removeAttributes(Instances instances) throws Exception {
        Remove remove = new Remove();
        remove.setAttributeIndices("1,6,7,16,17,26,27");
        remove.setInputFormat(instances);
        return Filter.useFilter(instances, remove);
    }

    private void printInstanceInfo(Instances instances) {
        int numAttributes = instances.numAttributes();
        System.out.println("The next lines displays all the attributes the loaded file contains");
        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute" + i + " = " + instances.attribute(i));
        }
    }

    private Instances loadArff(String dataInputFile) throws Exception {
        DataSource source = new DataSource(dataInputFile);
        Instances data = source.getDataSet();
        if (data.classIndex() == -1 )
            data.setClassIndex(data.numAttributes() - 1);
        return data;
    }

    private Instances csvWriter(String dataInputCommandline) throws Exception {
        PrintWriter writer = new PrintWriter(new File("testdata/csvfile.csv"));
        writer.write("ID,diagnosis, radius_mean, texture_mean, perimeter_mean, area_mean, smoothness_mean, compactness_mean, concavity_mean, concave_points_mean, symmetry_mean, fractal_dimention_mean, radius_SE, texture_SE, perimeter_SE, area_SE, smoothness_SE, compactness_SE, concavity_SE, concave_points_SE, symmetry_SE, fractal_dimention_SE, radius_worst, texture_worst, perimeter_worst, area_worst, smoothness_worst, compactness_worst, concavity_worst, concave_points_worst, symmetry_worst, fractal_dimention_worst");
        writer.write("\n");
        writer.write(dataInputCommandline);
        writer.flush();
        writer.close();
        DataSource source = new DataSource("testdata/csvfile.csv");
        return source.getDataSet();
    }

}
