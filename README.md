# CancerClassifier

This program is made to use machine learning algorithms to predict if an cancercell is malignant or benign

## Getting Started

Run the CommandLineMain.java file using the option:"-f testdata/unknownTestData.arff"
or if more help is needed use the -h or -HELP function


## Running the tests

To use the standard options run the program with: -f testdata/unknownTestData.arff 
or use -i 842517,?,20.57,17.77,132.9,1326,0.08474,0.07864,0.0869,0.07017,0.1812,0.05667,0.5435,0.7339,3.398,74.08,0.005225,0.01308,0.0186,0.0134,0.01389,0.003532,24.99,23.41,158.8,1956,0.1238,0.1866,0.2416,0.186,0.275,0.08902 

## Built With

IntelliJ

##Important links: 

data set: https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Original%29
Study: https://www.pnas.org/content/pnas/87/23/9193.full.pdf

## Author

* **Max Reneman** 

## License

Copyright (c) 2019 Max Reneman
All rights reserved

## Acknowledgments

* Michiel Noback for giving help if needed during the seminars


